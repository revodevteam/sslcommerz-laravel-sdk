<?php
declare(strict_types=1);

namespace App\Library\SslCommerz;

use Illuminate\Support\ServiceProvider;

final class SslCommerzServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        
    }

    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/stubs/config/sslcommerz.php' => config_path('sslcommerz.php'),
            __DIR__ . '/stubs/controller' => app_path('Http/Controllers'),
            __DIR__ . '/stubs/views' => resource_path('views'),
            __DIR__ . '/stubs/views' => resource_path('views'),
        ]);

        $this->copyRoutes();
    }

    private function copyRoutes(): void
    {
        $stubPath = __DIR__ . '/stubs/routes/web.php';
        $routePath = base_path('routes/web.php');

        if (str_contains(file_get_contents($routePath), 'SslCommerzPaymentController')) {
            return;
        }

        file_put_contents(
            $routePath,
            file_get_contents($stubPath),
            FILE_APPEND,
        );
    }
}
